import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import {
  MatButtonModule, MatCardModule, MatGridListModule, MatIconModule, MatListModule, MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import { ControlsService } from './services/controls/controls.service';
import { FeatureCardComponent } from './components/feature-card/feature-card.component';
import { HttpModule } from '@angular/http';
import { CustomTileComponent } from './components/custom-tile/custom-tile.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomTileComponent,
    FeatureCardComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatGridListModule,
    MatToolbarModule,
    MatCardModule,
    MatListModule
  ],
  providers: [
    ControlsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
