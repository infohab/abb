import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomTileComponent } from './custom-tile.component';

describe('CustomTileComponent', () => {
  let component: CustomTileComponent;
  let fixture: ComponentFixture<CustomTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
