import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ControlsService } from '../../services/controls/controls.service';

@Component({
  selector: 'app-custom-tile',
  templateUrl: './custom-tile.component.html',
  encapsulation: ViewEncapsulation.None
})
export class CustomTileComponent implements OnInit {

  private featureControls: any[];

  constructor(private controlsService: ControlsService) {
  }

  ngOnInit() {
    this.controlsService.getData()
      .then(response => {
        console.log(response);
        this.featureControls = response;
      });
  }
}
