import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-feature-card',
  templateUrl: './feature-card.component.html',
  encapsulation: ViewEncapsulation.None
})
export class FeatureCardComponent implements OnInit {

  @Input() data: any;

  constructor() {
  }

  ngOnInit() {

  }

  parseStatusClass(value: number): string {
    return value > 1 ? 'error' : value < 1 ? 'success' : 'warning';
  }

  parseIconClass(value: number): string {
    return value > 1 ? 'error' : value < 1 ? 'check circle' : 'warning';
  }
}
