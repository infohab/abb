import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ControlsService {

  private dataUrl = 'assets/dummy-data.json';

  /**
   * @description Function to parse the response json.
   * @param response
   * @returns {any[]}
   */
  private static extractData(response: Response): any[] {
    const body = response.json() as any[];
    return body || [];
  }

  /**
   * @description Function to deal with response errors.
   * @param error
   * @returns {any}
   */
  private static handleError(error: any): Promise<any> {
    console.error('controls.service: An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  constructor(private http: Http) {
  }

  getData(): Promise<any[]> {
    return this.http.get(this.dataUrl)
      .toPromise()
      .then(ControlsService.extractData)
      .catch(ControlsService.handleError);
  }
}
